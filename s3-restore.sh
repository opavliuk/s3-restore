#!/bin/bash
#
# title         : s3-restore.sh
# description   : This script for restore objects in AWS S3 Bucket.
# author        : Oleksii Pavliuk pavliuk.aleksey@gmail.com
# date          : 07/08/2020
# version       : 1.0.0
# usage         : Usage: ./s3-restore [OPTIONS]
# notes         : Before using this script, sure that you have full-access
#                 permissions to AWS S3.
#                 The script restores files by deleting Delete Markers and
#                 synchronizing objects from the replication bucket, if it exists.
#                 [!] The script wrote on the Mac OS system and can have
#                     undefined behavior on another operating system.

#
# -e: option instructs bash to immediately exit if
# any command has a non-zero exit status.
#
set -e

#
# The global main variables.
#
BUCKET=""

AWS_REGION=""
CLI_PROFILE="default"
LOG_FILE_PATH="./s3-add-replication.log"

WITHOUT_APPROVE=""

cmdname=$(basename $0)

#
# The global variable of objects which need to restore.
#
restore_objects=()

#
# ERROR Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - error messages
# Return   : None
#
echoerr() {
    date=$(date)
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@" 1>&2
}

#
# INFO Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - log messages
# Return   : None
#
log() {
    date=$(date)
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@"
}

#
# Usage.
# Show variables which you must or can set like arguments.
#
# Arguments: None
# Return   : None
#
usage() {
    cat <<USAGE >&2
Usage: ./$cmdname [OPTIONS]
  OPTIONS:
     [required]:
        --bucket ARG                  : a name of bucket
     [optional]:
        --aws-region ARG              : AWS Region [default: $AWS_REGION]
        --cli-profile ARG             : profile in AWS CLI [default: $CLI_PROFILE]
        --log-file-path ARG           : path to log file for logging [default: $LOG_FILE_PATH]
        --without-approve             : do all processes without user approve:
                                          * deleting all Delete Markers;
                                          * synchronizing buckets objects;
                                          * continue the process of synchronizing objects
                                            even replication rule is Disabled.
        -h | --help                   : show this usage

Example: ./$cmdname --bucket BUCKET_NAME
USAGE
    exit 1
}

#
# Checking connection to AWS S3.
#
# Arguments: None
# Return   : None
#
check_aws_s3_connection() {
    LOG_CASC_1="==> Connecting to AWS S3..."
    LOG_CASC_2="<== [!] Connected."

    log "$LOG_CASC_1"
    aws s3 ls 1>/dev/null
    log "$LOG_CASC_2"
}

#
# Checking connection to AWS Services.
#
# Arguments: None
# Return   : None
#
check_aws_connection() {
    LOG_CAC_1="=> Checking connection to AWS Services..."
    LOG_CAC_2="<= [!] Connections passed successfully."

    log "$LOG_CAC_1"
    check_aws_s3_connection
    log "$LOG_CAC_2"
}

#
# Request to delete all Delete Markers to AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
request_delete_markers() {
    LOG_RDM_1=">>> Request to delete all Delete Markers \
in bucket [$1]..."
    LOG_RDM_2="<<< [!] SUCCESS >>>"

    log "$LOG_RDM_1"
    aws s3api delete-objects \
        --bucket $1 \
        --delete "$(aws s3api list-object-versions \
            --bucket $1 \
            --output=json \
            --query='{Objects:
                                     DeleteMarkers[].{
                                          Key:Key,
                                          VersionId:VersionId
                                     }
                                  }')"
    log "$LOG_RDM_2"
}

#
# Request to get a replication bucket name
# in AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : "a name of replication bucket"
#
request_replication_bucket() {
    res=$(aws s3api get-bucket-replication \
        --bucket $1 \
        --output text \
        --query "ReplicationConfiguration.Rules[].Destination.Bucket")
    echo "${res##*:}"
}

#
# Request to get replication status for bucket
# in AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : "status"
#
request_replication_status() {
    res=$(aws s3api get-bucket-replication \
        --bucket $1 \
        --output text \
        --query "ReplicationConfiguration.Rules[].Status")
    echo "$res"
}

#
# Request to change replication rule
# status of the bucket to AWS S3.
#
# Arguments: $1 - a bucket name
#            $2 - status
# Return   : None
#
request_change_replication_status() {
    LOG_RCRS_1=">>> Requesting to change replication rule \
status for bucket [$1] to [$2]..."
    LOG_RCRS_2="<<< [!] SUCCESS >>>"

    log "$LOG_RCRS_1"
    aws s3api put-bucket-replication \
        --bucket $1 \
        --replication-configuration "$(aws s3api get-bucket-replication \
                                           --bucket $1 \
                                           --output json \
                                           --query '{Role: ReplicationConfiguration.Role,
                                                     Rules: ReplicationConfiguration.Rules[].{
                                                            Status:null,
                                                            Priority:Priority,
                                                            DeleteMarkerReplication:DeleteMarkerReplication,
                                                            Filter:Filter,
                                                            Destination:Destination}}' |  \
                                           sed "s/null/\"$2\"/g")"
    log "$LOG_RCRS_2"
}

#
# Approving request to user.
# Asks user to approve request.
# Uses only 'Y' or 'n'.
#
# Arguments: $1 - the main message
# Return   : true or false
#
approve_request() {
    LOG_AR_2="Please, use only 'Y' or 'n' to confirm! [Y/n]: "

    read -p "$1 $LOG_AR_2" var
    while True; do
        if [ $var == "n" ]; then
            echo "false"
            return
        elif [ $var == "Y" ]; then
            echo "true"
            return
        else
            read -p "$LOG_AR_2" var
        fi
    done
}

#
# Checking bucket in list of buckets in AWS S3.
#
# Arguments: $1 - a bucket name.
# Return   : true or false
#
check_list_buckets() {
    s3_buckets=$(aws s3api list-buckets \
        --output text \
        --query "Buckets[].Name")
    for item in ${s3_buckets[@]}; do
        if [ $item == $1 ]; then
            echo "true"
            return
        fi
    done
    echo "false"
}

#
# Checking bucket in list of buckets.
# Checks list of buckets. If bucket
# doesn't exist, it will offer to create one.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_bucket_exist() {
    LOG_CHB_1="=> Checking bucket [$1]..."
    LOG_CHB_2="<= [!] The bucket [$1] exists in AWS S3."
    LOG_CHB_3="<= [ERROR] The bucket [$1] DOESN'T exist in AWS S3.
To restore it from a replication bucket use script s3-add-replication.sh.
Command:
./s3-add-replication.sh --src-bucket $1 --dest-bucket REPLICA_BUCKET"

    log "$LOG_CHB_1"
    res=$(check_list_buckets $1)
    if [ $res == "false" ]; then
        echoerr "$LOG_CHB_3"
        exit 1
    fi
    log "$LOG_CHB_2"
}

#
# Finding all Delete Markers in bucket.
#
# Arguments: $1 - a bucket name
# Return   : None
#
find_all_delete_markers() {
    res_len=$(aws s3api list-object-versions \
        --bucket $1 \
        --output json \
        --query 'length(DeleteMarkers[].{Key: Key})')
    res=($(aws s3api list-object-versions \
        --bucket $1 \
        --output json \
        --query '[
                     DeleteMarkers[].{
                        Key: Key
                     }
                 ]'))
    res="${res[*]}"
    if [ "$res" == "[ null ]" ]; then
        echo ""
        return
    fi
    echo "$res" "$res_len"
}

#
# Delete Markers User Approving.
#
# Arguments: $1 - a bucket name
# Return   : None
#
delete_markers_approving() {
    LOG_DMA_1="""Confirm please to delete all Delete Markers \
in the bucket [$1] in AWS S3!
It needs for the process of restoring objects in the bucket. \
If you don't confirm, the process will stop."""
    LOG_DMA_2="[!] The Delete Markers WEREN'T DELETED in th bucket [$1]. \
The process of restoring objects was stopped."

    if [ ! -z $WITHOUT_APPROVE ]; then
        return
    fi

    res=$(approve_request "$LOG_DMA_1")
    if [ "$res" == "false" ]; then
        echoerr "$LOG_DMA_2"
        exit 1
    fi
}

#
# Checking the Delete Markers.
# Finding all Delete Markers in
# the bucket and deleting its.
#
# Arguments: $1 - a bucket name
#          : $2 - flag for repeating the check request
#                 (None or True)
# Return   : None
#
check_delete_markers() {
    LOG_CDM_1="=> Checking the Delete Markers in the bucket [$1]..."
    LOG_CDM_2="==> Searching the Delete Markers in the bucket [$1]..."
    LOG_CDM_3="<== [!] There ARE some Delete Markers in the bucket [$1]:"
    LOG_CDM_4="<== [!] There are NOT the Delete Markers in the bucket [$1]."
    LOG_CDM_5="<= [!] All Delete Markers in the bucket [$1] \
are checked and deleted."
    LOG_CDM_6="<== [ERROR] Something happened. The Delete Markers are NOT deleted. \
Check it out in AWS S3 Management Console!"

    log "$LOG_CDM_1"
    log "$LOG_CDM_2"
    delete_markers=($(find_all_delete_markers $1))
    if [[ ! -z "$delete_markers" && -z $2 ]]; then
        log "$LOG_CDM_3"
        log "Number of objects: ${delete_markers[${#delete_markers[@]}-1]}"
        unset delete_markers[${#delete_markers[@]}-1]
        log "${delete_markers[@]}"
        request_delete_markers $1
        check_delete_markers $1 "True"
        return
    elif [ ! -z "$delete_markers" ]; then
        echoerr "$LOG_CDM_6"
        exit 1
    fi
    log "$LOG_CDM_4"
    log "$LOG_CDM_5"
}

#
# Replication Status User Approve.
#
# Arguments: $1 - a replication status
# Return   : None
#
replication_status_approving() {
    LOG_RSA_1="""The replication rule is $1! It means that some objects may \
didn't replicate to the replication bucket.
Confirm please to continue process of restoring from replication bucket!"""
    LOG_RSA_2="[!] The replication rule is $1! \
The process of restoring objects from a replication bucket was canceled."

    log "Replication Rule Status: $1"

    if [[ ! -z $WITHOUT_APPROVE || "$1" == "Enabled" ]]; then
        return
    fi

    res=$(approve_request "$LOG_RSA_1")
    if [ "$res" == "false" ]; then
        echoerr "$LOG_RSA_2"
        exit 1
    fi
}

#
# Checking replication rule for bucket.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_replication_rule() {
    LOG_CRR_1="==> Checking replication rule for bucket [$1]..."
    LOG_CRR_2="<== [!] The replication rule exists."
    LOG_RRS_1=">>> Requesting replication status for bucket [$1]..."
    LOG_RRS_2="<<< [!] SUCCESS >>>"

    log "$LOG_CRR_1"
    log "$LOG_RRS_1"
    status=($(request_replication_status $1))
    log "$LOG_RRS_2"
    replication_status_approving $status
    log "$LOG_CRR_2"
}

#
# Checking the replication bucket exists.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_replication_bucket() {
    LOG_CRBE_1="==> Checking replication bucket [$1]..."
    LOG_CRBE_2="<== The replication bucket [$1] exists in AWS S3."
    LOG_CRBE_3="[ERROR] The replication bucket [$1] DOESN'T exist in AWS S3."

    log "$LOG_CRBE_1"
    res=$(check_list_buckets $1)
    if [ $res == "false" ]; then
        echoerr "$LOG_CRBE_3"
        exit 1
    fi
    log "$LOG_CRBE_2"
}

#
# Synchronization objects User Approve.
#
# Arguments: $1 - the main bucket name
#            $2 - a replication bucket name
# Return   : None
#
sync_objects_approving() {
    LOG_SOA_1="""Confirm please to synchronize objects from a replication \
bucket [$2] to the main bucket [$1]! Before start synchronization needs \
to disable a replication rule in the bucket for excluding \
replication the same object. After synchronization \
script will enable replication again."""
    LOG_SOA_2="[!] Synchronizing objects was canceled! \
The process of restoring objects from a replication bucket was canceled."

    if [ ! -z $WITHOUT_APPROVE ]; then
        return
    fi

    res=$(approve_request "$LOG_SOA_1")
    if [ "$res" == "false" ]; then
        echoerr "$LOG_SOA_2"
        exit 1
    fi
}

#
# Change Replication Rule Status for
# the main bucket.
#
# Arguments: $1 - a name of the main bucket
#            $2 - status (Enabled or Disabled)
# Return   : None
#
change_replication_status() {
    LOG_CRS_1="===> Changing status of replication rule \
for bucket [$1] to [$2]..."
    LOG_CRS_2="<=== [!] Status was changed."
    LOG_CRS_3="<=== [!] Status already is [$2]."

    log "$LOG_CRS_1"
    status=($(request_replication_status $1))
    if [ "$status" == "$2" ];
    then
        log "$LOG_CRS_3"
        return
    fi
    request_change_replication_status $1 $2
    log "$LOG_CRS_2"
    change_replication_status $1 $2
}

#
# Checking restore objects exist.
# If $restore_objects is NOT empty,
# function offers to synchronize buckets
# objects. And after this, it will check
# it out the process was completed
# successfully or not.
#
# Arguments: $1 - a name of the main bucket
#            $2 - a name of the replication bucket
#            $3 - ['error' or None] flag for echoerr() if
#                 $restore_objects is NOT empty
# Return   : None
#
check_restore_objects() {
    LOG_CRO_1="===> Checking restore objects exists..."
    LOG_CRO_2="<=== [!] There are NOT objects which need to restore. \
All is updated in the main bucket or doesn't exist in the replication bucket."
    LOG_CRO_3="[ERROR] Something happened. The synchronization \
objects is NOT completed well. Objects were NOT synchronized. \
Check it out in AWS S3 Management Console!"
    LOG_CRO_4="<=== [!] There ARE objects which need to restore:"

    log "$LOG_CRO_1"
    if [ -z "$restore_objects" ]; then
        log "$LOG_CRO_2"
        return
    fi
    log "$LOG_CRO_4"
    log "$(declare -p restore_objects)"

    if [[ ! -z $3 && "$3" == "error" ]];
    then
        echoerr "$LOG_CRO_3"
        exit 1
    fi

    change_replication_status $1 "Disabled"
    restore_objects $1 $2
    change_replication_status $1 "Enabled"
    get_restore_objects $1 $2
    check_restore_objects $1 $2 "error"
}

#
# Getting objects which need to restore.
# Gets all objects from 2 buckets and saves
# its in temporary files. Compare these files
# and gets files which need to restore and save
# its in ${restore_objects[@]}.
# Finally, deletes temporary files.
#
# Arguments: $1 - a source bucket
#            $2 - a destination bucket
# Return   : None
#
get_restore_objects() {
    unset restore_objects
    src_bucket_obj_file="$1_objects"
    dest_bucket_obj_file="$2_objects"

    LOG_GRO_1="===> Getting lists objects of buckets and save its \
in temporary files [$src_bucket_obj_file] and [$dest_bucket_obj_file]..."
    LOG_GRO_2="===> Comparing files of objects and getting objects which \
will need to restore..."
    LOG_GRO_3="<=== [!] The files of objects are compared."
    LOG_GRO_4="===> Deleting temporary files [$src_bucket_obj_file] and \
[$dest_bucket_obj_file]..."
    LOG_GRO_5="<=== [!] The temporary files are deleted."

    log "$LOG_GRO_1"
    aws s3 ls s3://$1 --recursive | awk '{$1=$2=$3=""; print $0}' | \
                                    sed 's/^[ ]*//' | \
                                    sort > $src_bucket_obj_file
    aws s3 ls s3://$2 --recursive | awk '{$1=$2=$3=""; print $0}' | \
                                    sed 's/^[ ]*//' | \
                                    sort > $dest_bucket_obj_file

    log "$LOG_GRO_2"
    while IFS= read -r line
    do
         restore_objects+=("$line")
    done < <( diff $src_bucket_obj_file $dest_bucket_obj_file \
                   --changed-group-format="%>"\
                   --unchanged-group-format="" )
    log "$LOG_GRO_3"

    log "$LOG_GRO_4"
    rm $src_bucket_obj_file
    rm $dest_bucket_obj_file
    log "$LOG_GRO_5"
}

#
# Restoring objects from a replication bucket
# to the main bucket.
#
# Arguments: $1 - the main bucket name
#            $2 - a replication bucket name
# Return   : None
#
restore_objects() {
    LOG_RO_1=">>> Coping restore objects from a replication \
bucket [$2] to the main bucket [$1]..."
    LOG_RO_2="<<< [!] SUCCESS >>>"

    log "$LOG_RO_1"
    len=${#restore_objects[@]}
    log "Number of objects: $len"
    for (( i=0; i<${len}; i++ ));
    do
        res=$(aws s3 cp "s3://$2/${restore_objects[$i]}" \
                        "s3://$1/${restore_objects[$i]}");
        log "$res"
    done
    log "$LOG_RO_2"
}

#
# Synchronizing objects between the main bucket
# and replication bucket.
#
# Arguments: $1 - a name of the main bucket
#            $2 - a name of the replication bucket
# Return   : None
#
sync_buckets() {
    LOG_SO_1="==> The process of synchronizing buckets \
[$2] => [$1] is started..."
    LOG_SO_2="<=== [!] Nothing will be restored. \
All is updated in the main bucket or doesn't exist in \
the replication bucket."
    LOG_SO_3="<== [!] The process of synchronizing buckets \
[$2] => [$1] is completed successfully."

    sync_objects_approving $1 $2
    log "$LOG_SO_1"
    get_restore_objects $1 $2
    check_restore_objects $1 $2
    log "$LOG_SO_3"
}

#
# Checking the replication bucket object.
# If replication rule and bucket exist,
# it will compare objects and offer
# to synchronize buckets for restore objects.
#
# Arguments: $1 - a name of the main bucket
# Return   : None
#
check_replica() {
    LOG_CR_1="=> Checking the replication bucket objects \
and comparing its..."
    LOG_CR_2="<= All objects are to-up-date."

    log "$LOG_CR_1"
    check_replication_rule $1
    replica_bucket=$(request_replication_bucket $1)
    check_replication_bucket $replica_bucket
    sync_buckets $1 $replica_bucket
    log "$LOG_CR_2"
}

#
# Main Function.
#
# Arguments: None
# Return   : None
#
main() {
    LOG_M_1="The process of restoring objects in AWS S3 \
bucket [$BUCKET] is started."
    LOG_M_2="[!] The process of restoring objects AWS S3 \
bucket [$BUCKET] is completed successfully."
    LOG_M_3="[        AWS Connection        ]"
    LOG_M_4="[        AWS S3 Buckets        ]"
    LOG_M_5="[        AWS S3 Delete Markers        ]"
    LOG_M_6="[        AWS S3 Replication        ]"

    log "$LOG_M_1"
    log "$LOG_M_3"
    check_aws_connection
    log "$LOG_M_4"
    check_bucket_exist $BUCKET
    log "$LOG_M_5"
    delete_markers_approving $BUCKET
    check_delete_markers $BUCKET
    log "$LOG_M_6"
    check_replica $BUCKET
    log "$LOG_M_2"
}

# --------------------| ARGUMENTS |--------------------

#
# Checking empty argument.
# If it isn't empty, the function will return an argument.
# Otherwise, it will show error message and usage.
#
# Arguments: $1 - argument
# Return   : argument
#
check_argument() {
    if [ ! -z $1 ]; then
        echo $1
    else
        echoerr "[ERROR] There is an empty argument!"
        usage
    fi
}

#
# Arguments Parsing.
# Parses arguments and give error with usage
# if argument didn't find.
#
while [ $# -gt 0 ]; do
    case "$1" in
    --bucket)
        shift 1
        BUCKET=$(check_argument $1)
        ;;
    --aws-region)
        shift 1
        AWS_REGION=$(check_argument $1)
        ;;
    --cli-profile)
        shift 1
        CLI_PROFILE=$(check_argument $1)
        ;;
    --log-file-path)
        shift 1
        LOG_FILE_PATH=$(check_argument $1)
        ;;
    --without-approve)
        WITHOUT_APPROVE="true"
        ;;
    -h | --help)
        usage
        ;;
    *)
        echoerr "Unknown argument: $1"
        usage
        ;;
    esac
    shift 1
done

#
# Required Arguments Checking.
#
if [ -z $BUCKET ]; then
    echoerr "[ERROR] Required argument is --bucket."
    usage
fi

main
